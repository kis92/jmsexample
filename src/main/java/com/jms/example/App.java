package com.jms.example;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by user on 03.02.17.
 */
public class App {
    public static void main(String[] args) {
        String url = "tcp://localhost:61616"; // url коннектора брокера
        try (
                Producer producer = new Producer(url);
                Consumer consumer = new Consumer(url, "test.in")
        ) {
            producer.start();
            consumer.init();

            BufferedReader rdr = new BufferedReader(new InputStreamReader(System.in));
            String line;
            while (!(line = rdr.readLine()).equalsIgnoreCase("stop")) // для выхода нужно набрать в консоли stop
            {
                producer.send(line);
            }
            System.out.println("Bye!");
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
