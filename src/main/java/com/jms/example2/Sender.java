package com.jms.example2;

import javax.jms.*;

/**
 * Created by user on 03.02.17.
 */
public class Sender {
    private Session session;
    private MessageProducer producer;
    private ConnectionUtil connectionUtil;

    Sender(String queue, boolean isTopic) throws JMSException {
        connectionUtil = new ConnectionUtil(queue);
        Destination destination;
        if(isTopic) {
            destination = connectionUtil.getDestinationTopic();
        }else{
            destination = connectionUtil.getDestinationQueue();
        }
        session = connectionUtil.getSession();
        producer = session.createProducer(destination);
    }

    public void sendMessage(String input) throws JMSException {
        TextMessage message =session.createTextMessage(input);
        producer.send(message);
    }

    public void stop(){
        connectionUtil.connectionClose();
    }
}
