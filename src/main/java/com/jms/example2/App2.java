package com.jms.example2;

import javax.jms.JMSException;
import java.util.Scanner;

/**
 * Created by user on 03.02.17.
 */
public class App2 {
    static String queue = "testTopic2";
    static boolean isTopic = true;

    public static void main(String[] args) throws JMSException {
        Receiver receiver = new Receiver(queue, isTopic);
        receiver.listenMessages();

        Sender sender  = new Sender(queue, isTopic);
        Scanner sc = new Scanner(System.in);
        String input = "";
        while (!input.equalsIgnoreCase("stop")){
            input = sc.nextLine();
            sender.sendMessage(input);
        }
        receiver.stop();
        sender.stop();
    }
}
