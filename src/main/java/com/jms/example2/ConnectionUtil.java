package com.jms.example2;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

/**
 * Created by user on 03.02.17.
 */
public class ConnectionUtil {
    private ActiveMQConnectionFactory connectionFactory=null; //управляемый объект от ApacheMQ
//служащий для создания объекта Connection.

    private Connection connection=null; //сам Connection.

    private Session session; //контекст для посылки и приема сообщений.

    private String queue=null; //имя очереди или топика.

    public ConnectionUtil(String queue) {
        this.queue = queue;
    }

    private static ActiveMQConnectionFactory getConnectionFactory(){
        return new ActiveMQConnectionFactory(
                ActiveMQConnectionFactory.DEFAULT_USER,
                ActiveMQConnectionFactory.DEFAULT_PASSWORD,
                ActiveMQConnectionFactory.DEFAULT_BROKER_URL
        );
    }

    public void connectionClose(){
        if(connection!=null){
            try {
                connection.close();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }

    public Session getSession() {
        return session;
    }

    public void connectionOpen(){
        try {
            if(connection==null){
                connectionFactory = getConnectionFactory();
                connection = connectionFactory.createConnection();
                connection.start();
                session =connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            }else{
                connection.start();
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }

    /**
     * Подключаемся к модели точка-точка.
     * @return
     */
    public Destination getDestinationQueue(){
        try {
            connectionOpen();
            return session.createQueue(queue);
        } catch (JMSException ex) {
            return null;
        }
    }

    /**
     * Подключаемся к модели подписчик/издатель.
     * @return
     */
    public Destination getDestinationTopic(){
        try {
            connectionOpen();
            return session.createTopic(queue);
        } catch (JMSException ex) {
            return null;
        }
    }

}
