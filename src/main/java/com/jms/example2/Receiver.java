package com.jms.example2;

import javax.jms.*;

/**
 * Created by user on 03.02.17.
 */
public class Receiver {
    private Session session;
    private MessageConsumer consumer;
    private ConnectionUtil connectionUtil;

    Receiver(String queue, boolean isTopic) throws JMSException {
        connectionUtil = new ConnectionUtil(queue);
        Destination destination;
        if (isTopic) {
            destination = connectionUtil.getDestinationTopic();
        } else {
            destination = connectionUtil.getDestinationQueue();
        }
        session = connectionUtil.getSession();
        consumer = session.createConsumer(destination);
    }

    public void listenMessages() throws JMSException {
        consumer.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                TextMessage textmessage = (TextMessage) message;
                try {
                    System.out.println("textmessage = " + textmessage.getText());
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public void stop(){
        connectionUtil.connectionClose();
    }
}
